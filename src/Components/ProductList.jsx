import React from "react";
import ProductItem from "./ProductItem";
const ProductList = ({ shoes, handleProductDetail }) => {
  console.log("shoes :", shoes);
  return (
    <div className="row container w-75 m-auto">
      {shoes.map((item) => (
        <ProductItem
          product={item}
          key={item.id}
          handleProductDetail={handleProductDetail}
        />
      ))}
    </div>
  );
};

export default ProductList;
