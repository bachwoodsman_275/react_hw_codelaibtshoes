import React from "react";

const Modal = ({ proDetail }) => {
  return (
    <div>
      <div>
        <div
          className="modal fade "
          id="exampleModal"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Xem chi tiết sản phẩm
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body d-flex">
                <div>
                  <img src={proDetail.image} alt="" />
                </div>
                <div>
                  <p className="font-weight-bold">{proDetail.name}</p>
                  <p className="my-3">{proDetail.price}$</p>
                  <p>{proDetail.shortDescription}</p>
                </div>
              </div>
              <div className="modal-footer">
                <button className="btn btn-secondary" data-dismiss="modal">
                  Close
                </button>
                <button className="btn btn-danger ">Save changes</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
