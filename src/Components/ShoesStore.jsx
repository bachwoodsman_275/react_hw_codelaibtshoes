import React, { useState } from "react";
import shoes from "./data.json";
import ProductList from "./ProductList";
import Modal from "./Modal";
const ShoesStore = () => {
  const [productDetail, setProductDetail] = useState(shoes[0]);
  const handleProductDetail = (product) => {
    console.log("product :", product);
    setProductDetail(product);
  };
  return (
    <div>
      <div className="text-center">SHOES SHOP</div>
      <ProductList shoes={shoes} handleProductDetail={handleProductDetail} />
      <Modal proDetail={productDetail} />
    </div>
  );
};

export default ShoesStore;
