import React from "react";

const ProductItem = ({ product, handleProductDetail }) => {
  return (
    <div className="col-4 mt-3 card">
      <div>
        <img className="w-100" src={product.image} alt="" />
      </div>

      <p className="font-weight-bold">{product.name}</p>

      <p className="my-3">{product.price}$</p>

      <button
        className="btn btn-dark mb-3"
        data-toggle="modal"
        data-target="#exampleModal"
        onClick={() => handleProductDetail(product)}
      >
        Xem chi tiết
      </button>
    </div>
  );
};

export default ProductItem;
